# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

[Jest cheatsheet](https://devhints.io/jest)\
[RTL cheatsheet](https://github.com/testing-library/react-testing-library/blob/main/other/cheat-sheet.pdf)\
[RTL queries](https://testing-playground.com/)


## Short Articles about react-testing-library

Learn about query method priority [query method priority](https://testing-library.com/docs/queries/about/#priority).\
Kent Dodds describes [common mistakes with react-testing-library](https://kentcdodds.com/blog/common-mistakes-with-react-testing-library).\
Robin Wieruch's [introduction to react-testing-library](https://www.robinwieruch.de/react-testing-library/).

## Example projects containing tests

Kent Dodds demo sandbox with [many different types of tests](https://codesandbox.io/s/github/kentcdodds/react-testing-library-examples/tree/main/?file=/src/__tests__/async.js).\
Todoist-clone with [tested out components](https://github.com/christyjacob4/react-todoist).


## Acceptance criteria for the progress bar

The Progress Bar should:

- show however many steps are provided in a horizontal Bar with a CheckBox and a TextField
- steps without a value should show an empty CheckBox and an ‘-’ in the Textfield
- steps without a value which correspond to the current step should show an empty ACTIVE CheckBox and an ‘-’ in the Textfield
- steps with a value should show a checked Checkbox and the respective value in the TextField
- steps are clickable if they have a value provided and if the step before them has a value provided
- the first step is clickable

(*What if your values are undefined?)