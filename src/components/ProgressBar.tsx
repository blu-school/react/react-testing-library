import * as React from "react";
import SectionCheck from "../images/SectionCheck.svg";
import SectionActive from "../images/SectionActive.svg";
import SectionInactive from "../images/SectionInactive.svg";

export interface Step {
  name: string | null;
  value: string | null;
  label: string | null;
}

interface IProgressBar {
  activeStep: number;
  setActiveStep: (value: number) => void;
  steps: Step[];
}

/**
 * Lists all available steps to be completed.
 */
export const ProgressBar: React.FC<IProgressBar> = (props: IProgressBar) => {
  const { steps, activeStep, setActiveStep } = props;

  const clickableStep = (idx: number): boolean => {
    return idx === 0 || (steps[idx - 1] && !!steps[idx - 1].value);
  };

  const onClickStep = (idx: number): void => {
    setActiveStep(idx);
  };

  return (
    <div className="progress_bar_container">
      <ul className="progress_bar">
        {steps?.map((step: Step, idx: number) => {
          return (
            <li key={"step-" + idx}>
              <button
                data-testid={"step-button-" + idx}
                onClick={() => onClickStep(idx)}
                disabled={!clickableStep(idx)}
              >
                {activeStep === idx && !step.value && (
                  <img
                    src={SectionActive}
                    alt="SectionActive"
                    data-testid={"img-section-active"}
                  />
                )}
                {activeStep !== idx && !step.value && (
                  <img
                    src={SectionInactive}
                    alt="SectionInactive"
                    data-testid={"img-section-inactive"}
                  />
                )}
                {step.value && (
                  <img
                    src={SectionCheck}
                    alt={"SectionCheck" + idx}
                    data-testid={"img-section-check-" + idx}
                  />
                )}
              </button>

              <div className="progress_bar_step_container">
                <div className="progress_bar_step_name active_black">
                  {step.name}
                </div>
                <div
                  data-testid={"step-value-" + idx}
                  className="progress_bar_step_value active_black"
                >
                  {step && step.value ? step.value : "-"}
                </div>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};
