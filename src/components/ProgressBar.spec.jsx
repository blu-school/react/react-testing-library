import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { ProgressBar } from './ProgressBar';

const activeStep = 1;
const steps = [
    { name: 'Type', value: 'Paid Vacation', label: null },
    { name: 'Dates', value: '23.1 - 29.3', label: null },
    { name: 'Approval', value: null, label: null },
    { name: 'Details', value: null, label: null }
];

describe('Progress bar', () => {
  it('should render with invalid props', () => {
    render(<ProgressBar activeStep={activeStep} setActiveStep={jest.fn()} steps={undefined} />);
  });

  it('should render placeholder if no value in respective step', () => {
    render(<ProgressBar activeStep={activeStep} setActiveStep={jest.fn()} steps={steps} />);

    const element = screen.getByTestId('step-value-3');

    expect(element.innerHTML).toBe("-");
  });

  it('should call callback with respective index', () => {
    const setActiveStepMock = jest.fn();

    render(<ProgressBar activeStep={activeStep} setActiveStep={setActiveStepMock} steps={steps} />);

    const element = screen.queryByTestId('step-button-1');

    fireEvent.click(element);
    
    expect(setActiveStepMock).toHaveBeenCalled();
    expect(setActiveStepMock).toHaveBeenCalledTimes(1);
    expect(setActiveStepMock).toHaveBeenCalledWith(1);
  });

  it('next step should not be clickable if there is no value for the current step', () => {
    const {rerender} = render(<ProgressBar activeStep={activeStep} setActiveStep={jest.fn()} steps={steps} />);
  
    const element = screen.queryByTestId('step-button-3');
   
    expect(element).toBeDisabled();

    const stepsCopy = [...steps];
    stepsCopy[2].value = 'gosho';

    rerender(<ProgressBar activeStep={activeStep} setActiveStep={jest.fn()} steps={stepsCopy} />);

    expect(element).not.toBeDisabled();
  });

  it('first step should always be clickable independently of value', () => {
    const {rerender} = render(<ProgressBar activeStep={activeStep} setActiveStep={jest.fn()} steps={steps} />);
  
    const element = screen.queryByTestId('step-button-0');

    expect(element).not.toBeDisabled();

    const stepsJSON = JSON.stringify(steps);
    const stepsCopy = JSON.parse(stepsJSON);

    stepsCopy[0].value = null;
    rerender(<ProgressBar activeStep={activeStep} setActiveStep={jest.fn()} steps={stepsCopy} />);

    expect(element).not.toBeDisabled();
  });

  it.only('show a SectionCheck symbol if there is a value for the current steps', () => {
    render(<ProgressBar activeStep={activeStep} setActiveStep={jest.fn()} steps={steps} />);
 
    const element = screen.getByAltText('SectionCheck0')
   
    expect(element).toBeInTheDocument();
  });

  // it('show a SectionActive symbol for the first step without value', () => {
  //   render(<ProgressBar activeStep={activeStep} setActiveStep={jest.fn()} steps={steps} />);
  // });

  // it('show a SectionInactive symbol for steps without a value which are not current step', () => {
  //   render(<ProgressBar activeStep={activeStep} setActiveStep={jest.fn()} steps={steps} />);
  // });
});
