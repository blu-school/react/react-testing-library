import { useState } from "react";
import { ProgressBar, Step } from "./ProgressBar";
import * as React from "react";

/**
 * Container component for the Progress Bar
 */
export const ProgressBarContainer: React.FC = () => {
  const [activeStep, setActiveStep] = useState(0);
  const steps: Step[] = [
    { name: "Type", value: "Paid urlauben", label: null },
    { name: "Dates", value: "fuer immer", label: null },
    { name: "Approval", value: "schauma", label: null },
    { name: "Details", value: null, label: null },
    { name: "Details", value: null, label: null },
  ];

  return (
    <>
      <div>{activeStep}</div>
      <ProgressBar
        activeStep={activeStep}
        setActiveStep={setActiveStep}
        steps={steps}
      />
    </>
  );
};
