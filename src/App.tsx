import React from "react";
import "./App.css";
import { ProgressBarContainer } from "./components/ProgressBarContainer";

function App() {
  return <ProgressBarContainer />;
}

export default App;
